import json
import os
from uuid import uuid4
import boto3

ddb = boto3.resource('dynamodb')
table = ddb.Table(os.environ['TODOS_TABLE_NAME'])


def createTodo(event, context):
    try:
        table.put_item(Item={'id': str(uuid4()), 'title': event["title"], 'description': event["description"]})
        return True
    except Exception as e:
        print("createTodo Error", e)
        return False


def getTodos(event, context):
    try:
        return table.scan()['Items']
    except Exception as e:
        print("getTodos Error", e)
        return None


def getTodo(event, context):
    # Assumption - purpusefully returned a single object instead of an array which always has length 1
    try:
        result = table.get_item(Key={'id': event["id"]})
        return result["Item"] if (result.get("Item") != None) else {}
    except Exception as e:
        print("getTodo Error", e)
        return None


def updateTodo(event, context):
    updateExpression = None
    updateValues = None
    if (event['title'] != None):
        updateExpression = 'SET title = :title'
        updateValues = {":title": event['title']}
    if (event['description'] != None):
        updateExpression = ', '.join([updateExpression, 'description = :description'])
        updateValues.update({":description": event['description']})
    try:
        if (updateExpression != None):
            table.update_item(
                Key={'id': event["id"]},
                UpdateExpression=updateExpression, ExpressionAttributeValues=updateValues)
        return True
    except Exception as e:
        print("updateTodo Error", e)
        return False


def deleteTodo(event, context):
    try:
        table.delete_item(Key={'id': event["id"]})
        return True
    except Exception as e:
        print("deleteTodo Error", e)
        return False
