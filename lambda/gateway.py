import json
import os

import boto3

ddb = boto3.resource('dynamodb')
_lambda = boto3.client('lambda')


def executeLambda(event=None):
    resp = _lambda.invoke(
        FunctionName=os.environ['ON_HANDLE_FUNCTION_NAME'],
        Payload=json.dumps(event if (event != None) else {}),
    )
    return json.loads(resp.get('Payload').read().decode("utf-8"))


def getReturnObj(statusCode: int, body: dict):
    return {
        'statusCode': statusCode,
        'headers': {
            'Content-Type': 'application/json'
        },
        'body': json.dumps(body)
    }


def getSuccessReturnObj(message: str):
    return getReturnObj(200, {"message": message})


def getErrorReturnObj(message: str):
    return getReturnObj(400, {"error": message})


def default(event, context):
    return getReturnObj(404, {"error": "Resource not defined"})


def getTodos(event, context):
    resp_json = executeLambda()

    if (resp_json != None):
        return getReturnObj(200, resp_json)
    else:
        return getErrorReturnObj("There was an error loading the To-Do objects.")


def getTodo(event, context):
    pathParams = event.get("pathParameters")
    resp_json = executeLambda({"id": pathParams.get("todo_id")})

    if (resp_json == {}):
        return getReturnObj(404, {"message": "To-Do with id: "+pathParams.get("todo_id")+" not found."})
    if (resp_json != None):
        return getReturnObj(200, resp_json)
    else:
        return getErrorReturnObj("There was an error loading the To-Do objects.")


def createTodo(event, context):
    params = json.loads(event.get("body"))
    resp_json = executeLambda({"title": params.get("title"), "description": params.get("description")})

    if (resp_json == True):
        return getSuccessReturnObj("To-Do object created successfully.")
    else:
        return getErrorReturnObj("There was an error while creating a new To-Do object.")


def updateTodo(event, context):
    # Assumption - if todo_id doesn't exist, create a new entry with the given id
    # Assumption - id is not changeable
    params = json.loads(event.get("body"))
    pathParams = event.get("pathParameters")
    resp_json = executeLambda(
        {"id": pathParams.get("todo_id"),
         "title": params.get("title"),
         "description": params.get("description")})

    if (resp_json == True):
        return getSuccessReturnObj("To-Do object updated successfully.")
    else:
        return getErrorReturnObj("There was an error while updating the To-Do object.")


def deleteTodo(event, context):
    # Assumption - if todo_id doesn't exist, return success
    pathParams = event.get("pathParameters")
    resp_json = executeLambda({"id": pathParams.get("todo_id")})

    if (resp_json == True):
        return getSuccessReturnObj("To-Do object deleted successfully.")
    else:
        return getErrorReturnObj("There has been an error while deleting the To-Do object.")
