#!/usr/bin/env python3
import os

import aws_cdk as cdk

from todo_api.todo_api_stack import TodoApiStack


app = cdk.App()
TodoApiStack(app, "TodoApiStack",)

app.synth()
