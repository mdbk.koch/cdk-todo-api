from aws_cdk import (
    RemovalPolicy,
    Stack,
    aws_lambda as _lambda,
    aws_apigateway as awsGateway,
    aws_dynamodb as dynamodb

)
from constructs import Construct
from todo_api.gateway import Gateway


class TodoApiStack(Stack):

    def __init__(self, scope: Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        Gateway(self, 'Gateway')
