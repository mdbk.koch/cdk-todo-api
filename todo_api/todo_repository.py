from constructs import Construct
from uuid import uuid4
from aws_cdk import (
    RemovalPolicy,
    aws_lambda as _lambda,
    aws_dynamodb as dynamodb
)


class TodoRepository(Construct):
    @property
    def tableName(self):
        return 'Todos'

    @property
    def table(self):
        return self._table

    @property
    def createTodo(self):
        return self._createTodo

    @property
    def getTodos(self):
        return self._getTodos

    @property
    def getTodo(self):
        return self._getTodo

    @property
    def updateTodo(self):
        return self._updateTodo

    @property
    def deleteTodo(self):
        return self._deleteTodo

    def __createLambdaFunction(self, id: str, handler: str):
        fn = _lambda.Function(
            self, id, runtime=_lambda.Runtime.PYTHON_3_9, code=_lambda.Code.from_asset('lambda'),
            handler=handler, environment={'TODOS_TABLE_NAME': self.table.table_name})
        self._table.grant_read_write_data(fn)
        return fn

    def __init__(self, scope: Construct, id: str, **kwargs):
        super().__init__(scope, id, **kwargs)

        self._table = dynamodb.Table(self, self.tableName, partition_key={
            'name': 'id', 'type': dynamodb.AttributeType.STRING}, removal_policy=RemovalPolicy.DESTROY)

        self._createTodo = self.__createLambdaFunction("CreateTodo", 'todo_repository.createTodo')
        self._getTodos = self.__createLambdaFunction("GetTodos", 'todo_repository.getTodos')
        self._getTodo = self.__createLambdaFunction("GetTodo", 'todo_repository.getTodo')
        self._updateTodo = self.__createLambdaFunction("UpdateTodo", 'todo_repository.updateTodo')
        self._deleteTodo = self.__createLambdaFunction("DeleteTodo", 'todo_repository.deleteTodo')
