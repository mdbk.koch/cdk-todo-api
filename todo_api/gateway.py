from os import environ
from constructs import Construct
from aws_cdk import (
    Resource,
    aws_lambda as _lambda,
    aws_apigateway as awsGateway
)

from todo_api.todo_repository import TodoRepository


class Gateway(Construct):
    def __addLambdaMethod(self, id: str, resource: Resource, handler: str, httpMethod: str,
                          onHandle: _lambda.IFunction):
        fn = _lambda.Function(
            self, id, runtime=_lambda.Runtime.PYTHON_3_9, code=_lambda.Code.from_asset('lambda'),
            handler=handler,
            environment={'ON_HANDLE_FUNCTION_NAME': onHandle.function_name})

        if (onHandle != None):
            onHandle.grant_invoke(fn)

        return {"lambda": fn, "apiMethod": resource.add_method(httpMethod, awsGateway.LambdaIntegration(fn))}

    def __init__(self, scope: Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        default = _lambda.Function(self, 'Default', runtime=_lambda.Runtime.PYTHON_3_9, code=_lambda.Code.from_asset(
            'lambda'), handler='todo.default')
        self._restApi = awsGateway.LambdaRestApi(self, 'Api', handler=default)

        # Create Routes
        self._versionedApiResource = self._restApi.root.add_resource('v1')
        self._todoApiResource = self._versionedApiResource.add_resource('todo')
        self._singleTodoApiResource = self._todoApiResource.add_resource('{todo_id}')

        todoRepo = TodoRepository(self, "TodoRepo")

        # Assign Routes to Lambdas
        self.__addLambdaMethod(id="GetTodos", resource=self._todoApiResource, handler='gateway.getTodos',
                               httpMethod='GET', onHandle=todoRepo.getTodos)
        self.__addLambdaMethod(
            id="CreateTodo", resource=self._todoApiResource, handler='gateway.createTodo', httpMethod='PUT',
            onHandle=todoRepo.createTodo)
        self.__addLambdaMethod(id="GetTodo", resource=self._singleTodoApiResource, handler='gateway.getTodo',
                               httpMethod='GET', onHandle=todoRepo.getTodo)
        self.__addLambdaMethod(
            id="UpdateTodo", resource=self._singleTodoApiResource, handler='gateway.updateTodo', httpMethod='PUT',
            onHandle=todoRepo.updateTodo)

        self.__addLambdaMethod(
            id="DeleteTodo", resource=self._singleTodoApiResource, handler='gateway.deleteTodo', httpMethod='DELETE',
            onHandle=todoRepo.deleteTodo)
